# -*- coding: utf-8; mode: org-development-elisp; -*-
#+title: org-development-core
#+subtitle: Part of the =org-development= Emacs Lisp package
#+author: =#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=
#+property: header-args :tangle org-development-core.el :lexical t
#+startup: nologdone show2levels
#+todo: TODO(t@) HOLD(h@/!) | DONE(d@)
#+todo: BUG(b@/!) | FIXED(x@)
#+todo: TEST(u) TEST-FAILED(f) | TEST-PASSED(p)
#+todo: DEPRECATED(r@) | OBSOLETE(o@)

* Introduction
** Dependencies
#+begin_src elisp :results none
(eval-when-compile (require 'custom))
(eval-when-compile (require 'mmxx-macros-anaphora))
(eval-when-compile (require 'mmxx-macros-introspection))
#+end_src

** Constants
#+begin_src elisp :results none
(defconst +org-babel-universal-markup-prefix+ "#+")
#+end_src

#+begin_src elisp :results none
(defconst +org-babel-universal-markup-prefix-length+
  (length +org-babel-universal-markup-prefix+))
#+end_src

#+begin_src elisp :results none
(defconst +org-babel-results-keyword-string+ "RESULTS")
#+end_src

#+begin_src elisp :results none
(defconst +org-babel-results-keyword-string-length+
  (length +org-babel-results-keyword-string+))
#+end_src

* Constants
#+begin_src elisp :results none
(defconst dvorak-left-hand-convenient-keys '("u" "e" "o" "a" "i"))
(defconst qwerty-left-hand-convenient-keys '("f" "d" "s" "a" "g"))
#+end_src

* Customizable: group org-development
#+begin_src elisp :results none
(defgroup org-development nil
  "Software development in Org files"
  :tag "Org Development"
  :prefix "org-development-"
  :group 'org
  :link '(url-link :tag "GitLab repository"
                   "https://gitlab.com/akater/org-development")
  :link `(url-link :tag "Send Bug Report"
                   ,(concat "mailto:" "nuclearspace@gmail.com" "?subject=\
org-development bug: \
&body=Describe bug here, starting with `emacs -Q'.  \
Don't forget to mention your Emacs and `org-development' versions.")))
#+end_src

* Variable
#+begin_src elisp :results none
(defvar-local org-development-mode nil)
#+end_src

* context-is-primary-definition-block
** Prerequisites
*** stringp+
#+begin_src elisp :results none
(eval-when-compile (define-predicate+ stringp))
#+end_src

** Definition
#+begin_src elisp :results none
(defun o-d-context-is-primary-definition-block (language)
  ;; totally ad hoc and only works for elisp now
  ;; but in the end primary-definition-block will be an entity
  ;; definable in modes derived from org-development-mode
  ;; within general spec that defines cells
  ;; cells' headers (and corresponding header cycling endomorphisms)
  ;; will be generated from that spec too
  (cl-ecase language
    ((emacs-lisp elisp)
     (let ((context (org-element-at-point)))
       (aand (string= "emacs-lisp" (plist-get (cadr context) :language))
             (stringp+ (plist-get (cadr context) :parameters))
             (string= ":results none" it)
             ;; this is non-nil, as org-ctrl-c-ctrl-c-hook requires:
             context)))))
#+end_src
