;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'org-development
  authors "Dima Akater"
  first-publication-year-as-string "2020"
  org-files-in-order '("org-development-core"
                       "org-development-util"
                       "org-development-cells"
                       "org-development-test"
                       "org-development-mode"
                       "org-development")
  org-files-for-testing-in-order '("org-development-tests")
  site-lisp-config-prefix "50"
  license "GPL-3")
