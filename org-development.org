# -*- coding: utf-8; mode: org-development-elisp; -*-
#+title: org-development
#+subtitle: “Main file” for =org-development= Emacs Lisp package
#+author: =#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=
#+property: header-args :tangle org-development.el :lexical t
#+startup: nologdone show2levels
#+todo: TODO(t@) HOLD(h@/!) | DONE(d@)
#+todo: BUG(b@/!) | FIXED(x@)
#+todo: TEST(u) TEST-FAILED(f) | TEST-PASSED(p)
#+todo: DEPRECATED(r@) | OBSOLETE(o@)

* Overview
Elisp package that streamlines software development in Org, namely in single-language org files.  Multi-language Org development is out of scope of =org-development= for now.

A typical =org-development=-derived library would provide
- language-specific headers (here we make significant use of the “single language” assumption).
- aid in sectioning
- aid in metadata declaration (particularly, dependency declarations)

** Introduction
~org-development~ packages several elements of my org-based development workflow.  The workflow is idiosyncratic but similar ideas had been implemented, with moderate public success, in [[https://nbdev.fast.ai][nbdev]].

Some implementation details of ~org-development~ are Elisp- and Org- specific but many parts of the overall protocol could be implemented on top of some other notebook interface.  Essentially, ~org-development~ is an implementation of notebook interface in Emacs+Org.

** The Notebook Workflow
The workflow is centered around the following ideas:

- Free-form REPL
- Natural tests
- Blurring the line between code and documentation

Free-form REPL is an idea fairly well known to Emacs users: Elisp buffers a la =*scratch*= allow to write a form anywhere, evaluate it and have the result recorded nearby.

However, one cannot really write and evaluate forms this way right in the source file while keep it a working source file: we don't want many of the forms we write to evaluate when the file is loaded.  Still, it is beneficial to write them and also to keep them, together with their results.

To address the issue, one could have a macro like
#+begin_src elisp :tangle no :results code :wrap example elisp
(execute-only
 (test something))
#+end_src

where ~(execute-only ..)~ is equivalent to ~(cl-eval-when (execute) ..)~.  Auto-wrapping ~execute-only~ around expressions on their evaluation could be enabled, maybe depending on the evaluation command used.  ~outline-minor-mode~ could be employed for more convenient sectioning and navigation.  This would be another implementation of notebook interface, in pure lisp-mode or pure elisp-mode.

Rather than
#+begin_src elisp :tangle no :results code :wrap example elisp
(define something)

(define something-else)

(define yet-something-else)
#+end_src

The source file would look like

#+begin_src elisp :tangle no :results code :wrap example elisp
(define something)

(execute-only
 (test something))

.. (results ..) or whatever ..

(define something-else)

(execute-only
 (test something-else))

.. (results ..) or whatever ..

(define yet-something-else)
#+end_src

and so on.  ~execute-only~ forms would be “test cells”, remaining ones would be “code cells”.

There are several downsides to this, including but not limited to:
- being Lisp-specific
- (ergo) supporting single language only
- outline-minor-mode being limited in features

~org-development~ leverages org-mode to get better cells and better infrastructure around them.

** The Notebook Workflow in Org-mode
*** Tests-driven ~org-development~
My core motivation is: I like it when tests and their results are present in a very straightforward way, and are easily available to users of a library or a package.

*Examples* of usage of a function, or an interface, are special cases of *Tests*: there's a form to be evaluated, there's expected result.  The only difference is, examples show up in documentation while generic tests do not.

#+name: consistency of documentation and code
Furthermore, if examples are formalized as any other tests, guaranteed is consistency of documentation and code, at least to the extent of the coverage of code by examples.

Thus, a typical section in an org-development file where a Lisp function (a macro ~sow~, in this case) is defined, looks like this:
#+begin_example org
,** Summary...
,** Examples
[...]
,*** TEST-PASSED Sow into different targets
,#+begin_src lisp :tangle no :load no :results value verbatim :wrap example lisp :package serere-tests
(reap ((:squares :cubes))
  (loop :for i :from 1 :to 10
        :sum (1+ (- (sow (expt i 3) :cubes)
                    (sow (expt i 2) :squares)))))
,#+end_src

,#+EXPECTED:
,#+begin_example lisp
2650
(1 4 9 16 25 36 49 64 81 100)
(1 8 27 64 125 216 343 512 729 1000)
,#+end_example
#+end_example

There's a short title describing what code in the example does, and there's expected result.  Note that we leverage many features of Org-mode: TODO headers, src blocks, src blocks' headers, labels.

If I run =M-x run-nearest-test=, normally, I'll get (an equivalent of) “Test passed” message.  Otherwise, I'll see something like this:
#+begin_example org
,**** TEST-FAILED Sow into different targets
,#+begin_src lisp :tangle no :load no :results value verbatim :wrap example lisp :package serere-tests
(reap ((:squares :cubes))
  (loop :for i :from 1 :to 10
        :sum (1+ (- (sow (expt i 3) :cubes)
                    (sow (expt i 2) :squares)))))
,#+end_src

,#+RESULTS:
,#+begin_example lisp
2650
,#+end_example

,#+EXPECTED:
,#+begin_example lisp
2650
(1 4 9 16 25 36 49 64 81 100)
(1 8 27 64 125 216 343 512 729 1000)
,#+end_example
#+end_example

Naturally, there's a single command to run all tests in the buffer to get informative diff about what's changed.  Since this is Emacs, the diff buffer will be interactive, with links to the org file in question.  One can jump and fix the issues, or commit as is — commit diffs will contain explicit descriptions of which tests have broken.

Sometimes a working peace of code can be turned into a test as is.  E.g. in [[file:../emacs-console-colors/console-colors-core.org::*material table][this section]] I used to generate content at build time, and at a certain moment the code blocks got formalized as tests.  I call such action “to test-envelope a piece of code”.  I don't think there's an equivalent to this action in non-notebook workflows.

One could describe all this as TDD, I guess.  I'm not familiar with that culture and the term enough to use it.

We might integrate this test facility with various testing frameworks from various languages but that's a low priority task for me personally.

*** Free-form REPL
As demonstrated above, test forms are written right in the main Org file, evaluated there, results are recorded, expected results are preserved if new results don't match them.

Non-test forms, that is, forms that are supposed to be evaluated at load time or compile time, are enclosed in different src block headings:
#+begin_example org
,#+begin_src elisp :results none
(defun square (x) (* x x))
,#+end_src
#+end_example

Corresponding results are not recorded.  Headings differ depending on the language in use and can be conveniently cycled through.

*** Blurring the line between code and documentation
To a certain extent, this feature is already evident, see [[consistency of documentation and code]].  However, our goal is more ambitious: to make (an equivalent of) =C-h f= drop user to a dedicated editable but throwaway Org buffer with interactive examples that can be easily messed with to quickly achieve the desired effect.  This is, in our experience, the most efficient way to use the documentation.  The buffer in question could merely contain a copy of the corresponding section in the original code if it's written well.  ~org-development~ should provide tools to make it easy to produce such well-written sections and to ensure their consistency; so far it mostly does not.

** “Literate programming”
*** ~org-development~ has nothing to do with literate programming
~org-development~ distances itself from the notion of “literate programming”.  =nbdev= employs the term “exploratory programming” which is exactly the term we were using since 8 years ago so it looks like a good fit.  =nbdev= also mentions Knuth's term in promotional materials but this could be due to the fact that those are promotional materials, and it does look like there is a need for different term.

The reasons for staying away from the term are,

- It doesn't mean anything: no protocol is specified.  A vague idea is basically useless, as is well known to programmers.
- If we nevertheless try to ascribe some meaning to, and find distinguishing features of LP, it'd include “write text to be tangled into source code and weaved into documentation”.  ~org-development~ doesn't weave anything; in case of Common Lisp we don't tangle anything either, and I don't think this whole idea is good.
- LP doesn't seem to work as intended, not only for inexperienced afficionados but for its designer as well, see [[akkaritik-2014]] for example.
- The designer writes (wrote) programs in a very peculiar way: pencil & paper, proof of correctness, convert to Pascal.  Never had a common dev experience, let alone team experience.  LP itself was arguably designed, or at least motivated thereof, to circumvent his university's copyright claims: university had claims re: code but not re: books.  All this affected the design negatively.
- Besides not having a common dev experience, the designer apparently was not exposed to actually good tools for writing clear code.  If one goal of LP is to have understandable code, this goal can and should be achieved by means of the language (preferrably) or at least by means of toolchain (when language sucks).  Lisp of course allows you to write code any way you want, *literally* (no pun intended).

You may find some other points [[jones-quora-2018][here]].

I have to reiterate, despite me sayng “LP was designed…”, I haven't yet actually seen any design that I could implement — say, in a build system.  I tried; I only could implement some vague ideas but not a spec, not a protocol.

Hence, I don't think of what I'm doing as of LP.  Instead, I just say I use Org to implement a Notebook Interface.

The goal of orgdev is /interactive/ programming, not “literate”.  The goal is to shrink the gap between a file and a repl in the direction of the latter — creating such a workflow as if files did not exist at all.  (It's unfortunate that they do.)

*** Links
**** akkaritik-2014
:PROPERTIES:
:URL:      http://akkartik.name/post/literate-programming
:ID:       899a07fd-c954-4230-836e-b066478f0757
:ARCHIVED_AT: [[file:data/89/9a07fd-c954-4230-836e-b066478f0757/2022-01-03T22:51:47+0000/][2022-01-03T22:51:47+0000]]
:END:

** Installation
*** Gentoo
To install, please
#+begin_src sh :dir /sudo::/ :tangle no :results none
eselect repository enable akater && \
emerge app-emacs/org-development
#+end_src

*** Other systems
**** TODO Use package-vc (Emacs 29+)
- State "TODO"       from              [2023-06-27 Tue 12:45]
**** Install manually
Ensure the following dependencies are installed into your system-wide site-lisp dir:
- [[https://framagit.org/akater/elisp-mmxx-macros][mmxx-macros]] (only necessary at build time)
- [[https://gitlab.com/akater/elisp-set-endos][set-endos]] (only necessary at build time)
# - [[https://gitlab.com/akater/elisp-akater-misc][akater-misc]] (only necessary when enable-colored)
- [[https://gitlab.com/akater/org-header-cycling][org-header-cycling]]
- [[https://gitlab.com/akater/etemplate][etemplate]]
- [[https://framagit.org/akater/org-run-tests][ort]]
- [[https://github.com/polymode/poly-org][poly-org]] (0.2 and higher)

Clone ~org-development~, switch to =release= branch.

~org-development~ can be installed into system-wide directory =/usr/share/emacs/site-lisp= (or wherever your ~SITELISP~ envvar points to) with
#+begin_src sh :tangle no :results none
make && sudo make install
#+end_src
but it's best to define a package for your package manager to do this instead.

If your package manager can't do that, and you don't want to install with elevated permissions, do
#+begin_src sh :tangle no :results none
SITELISP=~/.config/emacs/lisp SITEETC=~/.config/emacs/etc make && \
DESTDIR=~/.config/emacs SITELISP=/lisp SITEETC=/etc make install
#+end_src
(you may simply eval this block with =C-c C-c=; it won't be informative if it fails but hopefully it'll work)

In any case, you'll have to add load-path to your config, as demonstrated in [[Suggested config]].

If you don't have a system-wide site-lisp directory where dependencies are found in versionless directories, you may evaluate [[elisp:(url-retrieve"https://framagit.org/akater/emacs-fakemake/-/raw/master/fakemake-make.org"(lambda(_ b &rest __)(eww-parse-headers)(let((rb (current-buffer))(start(point)))(with-current-buffer b(let((inhibit-read-only t))(erase-buffer)(insert-buffer-substring rb start)(goto-char(point-min))(eww-display-raw b'utf-8)(org-mode))(goto-char(org-babel-find-named-block"defun bootstrap-sitelisp"))(pop-to-buffer b)(recenter 1))))(list(get-buffer-create"https+org")))][this block]] and use the command defined there to create a symlink to org (and other packages that you use) in “site-lisp directory to fill” which you may specify.  That directory should be specified as ~SITELISP~ then.  If the link above is scary, find bootstrap-sitelisp manually [[https://framagit.org/akater/emacs-fakemake/-/raw/master/fakemake-make.org][here]].

*** Notes for contiributors
If you have a local version of repository, compile with something like
#+begin_example sh :tangle no :results none
ORG_LOCAL_SOURCES_DIRECTORY="/home/contributor/src/org-development"
#+end_example

This will link definitions to your local Org files.

** Suggested config
#+begin_example elisp
(use-package org-development :ensure nil :defer t
  :load-path "/usr/share/emacs/site-lisp/org-development"
  ;; or maybe
  ;; :load-path "~/.config/emacs/lisp/org-development"
  )
#+end_example

* Comparison with =nbdev= features
While we're at it, let's go through =nbdev='s own table of contents and compare the design principles.

** TODO Cells
- State "TODO"       from              [2021-03-15 Mon 13:19]
** TODO Tests
- State "TODO"       from              [2021-03-15 Mon 13:19]
** Configuring
One benefit of notebook interface not explored by nbdev is its ability to describe and (partially or completely) implement configuration options for the system (library, etc.) written with the notebook interface.  E.g., cells could be tangled or not conditionally, and conditions could be described in cells themselves.

It is beneficial to have a very simple interface to this.  Many org exporting options may be specified in dynamic variables.  Configuration options, however, can be arbitrary complicated so it's better to have a dynamic variable that references one global list of options, with no specification other than it should be a list.  A cell could then be configured like this:

#+begin_example org
,#+begin_src elisp :results none :tangle (if (memq 'do-define-everything-at-once global-options-list) "filename" "")
(define everything at once)
,#+end_src
#+end_example

However, a file that uses such specifications can't be tangled if the variable (~global-options-list~ in the example above) is unbound.  It could be defined by dir-local or buffer-local variables but we find those distracting and intrusive.  Arguably, one such general purpose variable should be provided by Org itself.  We also want it to have a short name to minimize visual clutter.  Given all this, until such a variable is defined in org-mode, org-development will define such a variable when it is loaded itself:
#+begin_src elisp :results none
(defvar-local ob-flags nil)
#+end_src

We use ~ob-~ prefix, as in ~org-babel~.  This is a little intrusive but we still prefer this to shoving local variables into templates.  We could offer to defvar it in suggested config but we would also have to put the whole raison d'etre into config, more or less re-telling this whole section; otherwise it'd be very confusing to those who read configs or copy them.  So we settled on this.  But, again, in our view org-mode should have it built-in.

* Some comparisons with design principles of nbdev
** Possibly many languages per file
Unlike =nbdev=, we do not limit ourselves to development for single language.  As far as we know, when it comes to multi-language notebooks in =nbdev=, there is a limitation of Jupyter in play.  ~org-development~ provides some (limited at the moment) interface to writing language-specific derived ~org-development-~ modes.  It is also possible to combine several languages in a single org-development project but such functionality will only get mature after individual derived modes do.  For now we only have ~-elisp~ and ~-lisp~ language specific modes.

At the moment, I limit myself to the case of a single language per org file though.  I'd like to support multiple languages per file but I have basically zero vision w.r.t how this should be implemented, so far.

~org-headers-cycling~ will work for any number of languages per file but one would have to combine features from various ~org-development-~ derived modes which are major modes.  So this issue looks delicate.  We might have to make derived modes minor, keeping ~org-development~ itself a major mode, but we'd like to use inheritance, and it looks like mode inheritance mechanisms of Elisp only produce major modes.

** TODO Export to modules
- State "TODO"       from              [2021-03-02 Tue 22:58]
Some modes can avoid any sort of exporting or tangling, in particular ~org-development-lisp~ presumes org files are read as is.
** TODO Synchronize and diff
- State "TODO"       from              [2021-03-02 Tue 22:58]
** TODO Show doc
- State "TODO"       from              [2021-03-02 Tue 22:58]
** TODO Convert to html
- State "TODO"       from              [2021-03-02 Tue 22:58]
** TODO Extract tests
- State "TODO"       from              [2021-03-02 Tue 22:58]
Tests are not “extracted” in org development but this may be a mere terminological difference, we need to investigate.

** TODO Fix merge conflicts
- State "TODO"       from              [2021-03-02 Tue 22:58]
** TODO Command line functions
- State "TODO"       from              [2021-03-02 Tue 22:58]
** Clean notebooks
This is not an issue with Org.

** TODO Setting up Search
- State "TODO"       from              [2021-03-02 Tue 22:58]
* Issues
** TODO Improve imenu
- State "TODO"       from              [2021-03-11 Thu 05:42] \\
  Either make imenu look for (and format properly) #+name: labels in org-development buffers, or call imenu in indirect buffer created by poly-org
** TODO test org-project-clean
  - State "TODO"       from              [2020-02-25 Tue 17:03]
** TODO try to complete org-development-run-tests
  - State "TODO"       from              [2020-02-20 Thu 03:01]
** TODO dispatch C-c C-c depending on the block header
  - State "TODO"       from "TODO"       [2020-02-24 Mon 19:30] \\
    …both with point on header and with point inside the block (poly-org presumed). e.g. lisp definition blocks should use ~slime-compile-defun~.
  - State "TODO"       from              [2020-02-20 Thu 02:54]
** TODO function that creates a new example cell after the current one and moves point there
  - State "TODO"       from              [2020-02-19 Wed 18:11]
** TODO function that forks whole definition and examples sections
  - State "TODO"       from              [2020-02-25 Tue 01:19]
** TODO function that cycles headers conveniently in the whole section
  - State "TODO"       from              [2020-02-25 Tue 01:22] \\
    definition to muted, or vice versa
** TODO add template with standard melpa prelude to org-development-elisp
  - State "TODO"       from              [2020-02-18 Tue 01:54]
** TODO generate ebuild while creating a project in org-development-elisp
  - State "TODO"       from              [2020-02-18 Tue 02:14]
** WAIT add org-babel-cycle-src-block-header to org-header-cycling
  - State "WAIT"       from "TODO"       [2020-02-20 Thu 03:05] \\
    It is unclear how to do it. Better wait for use cases
for general points
** TODO processing functions that extract prerequisites from OBSOLETE sections before their removal
  - State "TODO"       from              [2020-02-19 Wed 02:09] \\
    this goes into org-structure territory. so far, org-structure specifies hierarchy, but org-development specifies org todo keyword sequences
** TODO get akater-lisp-split-sexp-maybe-demarcate-org-block to org-development (as contrib, as it's ugly)
  - State "TODO"       from              [2020-02-20 Thu 03:22]
** TODO use lice.el to conveniently insert license texts into org blocks
  - State "TODO"       from "TODO"       [2020-02-20 Thu 02:59] \\
    For starters, we need to provide functions that will insert licenses into src blocks or into main body of an org file. Then use this interface to augment templates if user likes it this way
  - State "TODO"       from              [2020-02-19 Wed 02:36]
** TODO output the details of failed test
- State "TODO"       from "TODO"       [2022-01-03 Mon 20:03] \\
  A better approach is to wrap code of the block into error catcher.  E.g. auto-wrap everything Elisp in ~orgdev-maybe-return-error~ (which should thus be renamed to ~orgdev-elisp-maybe-return-error~).
  - State "TODO"       from              [2020-02-19 Wed 19:45] \\
    in a src block #+info after #+expected or maybe in a tag (e.g. undefined-function)
** TODO open an issue: narrow-to-region in poly-org does not narrow indirect buffers
  - State "TODO"       from              [2020-02-20 Thu 02:16]
** TODO provide a converter from usual source to org
  - State "TODO"       from              [2020-02-20 Thu 03:18] \\
  on the surface, this is just “inverse comments”
** TODO make sure org-development works with org-evil
  - State "TODO"       from              [2020-02-17 Mon 16:34]
** TODO org-development
   - State "TODO"       from "TODO"       [2020-02-28 Fri 03:48] \\
     I need to decide on aliases for generic functions
   - State "TODO"       from "TODO"       [2020-02-18 Tue 01:49] \\
     no separate project-stubs
   - State "TODO"       from "HOLD"       [2020-02-15 Sat 00:41] \\
     org-project-stubs is the main issue now, and it's not clear how to implement (how to find blobs, in particular)
*** provides
provides framework for programming org workflows for different (mainly interactive but not only interactive) languages
*** depends on
- org-header-cycling
- org-doc-center (?)
** HOLD (long shot) org-doc-center
that's a long-term thing but zevlg mentioned some existing attempts
*** TODO org-doc-center pages need a different visual color scheme
   - State "TODO"       from              [2020-02-20 Thu 02:28] \\
so that developers don't accidentally start writing their work there by mistaking org-doc-center page with their source file
* Dependencies
#+begin_src elisp :results none
(eval-when-compile (require 'cl-macs))
(require 'etemplate)
(require 'org-development-core)
(require 'org-development-mode)
(require 'org-development-test)
(require 'org-development-cells)
(require 'org-development-util) ; org-development's task is to provide these utils as well
#+end_src

* org-project aliases
In the future, ~org-development~ might define its own subclass(es) of ~etemplate--project~.  So far, it merely defines aliases so that derived modes' implementors don't have to require a package that is too general for their purposes (~org-project~) which would be conceptually wrong.  ~org-development~ should provide its own interface, isolated from ~org-development~'s own dependencies.
#+begin_src elisp :results none
(defalias 'define-org-development-project-class
  'define-eproject-class)
(defalias 'org-development-project-directory
  'eproject-directory)
(defalias 'org-development-project-initialize
  'etemplate-initialize)
(defalias 'org-development-project-clean
  'eproject-clean)
#+end_src

* org-development-quick-and-dirty-setup
** Summary
#+begin_src elisp :tangle no :eval never
(org-development-quick-and-dirty-setup mode)
#+end_src
On /mode/ activation, set (locally) ~org-header-endomorphisms-list~ and ~org-structure-template-alist~.

** Examples
*** Basic Examples
**** TEST-PASSED Trivial ~org-development-quick-and-dirty-setup~ example
#+begin_src elisp :tangle no :results code :wrap example elisp :keep-expected t
(macroexpand-1
 `(org-development-quick-and-dirty-setup org-development-elisp))
#+end_src

#+EXPECTED:
#+begin_example elisp
(eval-and-compile
  (defun org-development-elisp-setup nil
    (setq-local org-header-cycling-endomorphisms-list
                (list org-development-elisp-headers-endomorphism)
                org-structure-template-alist
                (org-development-template-alist-from-endomorphism
                 org-development-elisp-headers-endomorphism)))
  (defun org-development-elisp-unload-function nil
    (setf org-development-elisp-mode-hook
          (delq 'org-development-elisp-setup
                org-development-elisp-mode-hook)))
  (add-to-list 'org-development-elisp-mode-hook 'org-development-elisp-setup)
  (defun org-development-elisp-make-buffer (&optional name)
    "Make throwaway buffer set up for `org-development-elisp-mode' and display it.

Use NAME for buffer name; ask for it and append “.org” when NAME is nil."
    (interactive)
    (orgdev--new-buffer :name name :display t :coding 'utf-8
                        :mode 'org-development-elisp :offer-save nil)))
#+end_example

** Notes
Here, we only allow to fill org-header-endomorphisms-list with a single endomorphism. See [[Single language]].

** Definition
#+begin_src elisp :results none
(cl-defmacro org-development-quick-and-dirty-setup
    (feature &aux
             (feature-name (symbol-name feature))
             (headers-endomorphism-variable-name
              (intern (concat feature-name "-" "headers-endomorphism")))
             (setup-function-name
              (intern (concat feature-name "-" "setup")))
             (unload-function-name
              (intern (concat feature-name "-" "unload-function")))
             (make-buffer-function-name
              (intern (concat feature-name "-" "make-buffer")))
             (hook-name
              (intern (concat feature-name "-" "mode-hook"))))
  `(eval-and-compile
     (defun ,setup-function-name ()
       (setq-local org-header-cycling-endomorphisms-list
                   (list ,headers-endomorphism-variable-name)
                   org-structure-template-alist
                   (org-development-template-alist-from-endomorphism
                    ,headers-endomorphism-variable-name)))
     (defun ,unload-function-name ()
       (setf ,hook-name
             (delq ',setup-function-name ,hook-name)))
     (add-to-list ',hook-name ',setup-function-name)
     ;; (autoload ,make-buffer-function-name
     ;;   (file-name-nondirectory (buffer-file-name (current-buffer))))
     (defun ,make-buffer-function-name (&optional name)
       ,(format "Make throwaway buffer set up for `%s-mode' and display it.

Use NAME for buffer name; ask for it and append “.org” when NAME is nil."
                feature)
       (interactive)
       (orgdev--new-buffer :name name :display t
                           :coding 'utf-8 :mode ',feature
                           :offer-save nil))))
#+end_src
